# congo-custom-template
A congo website with some customization on its design.

## Prerequisites
- Git
- Go (version 1.12 or later)
- Hugo extended (version 0.87.0 or later)

## Setting up

1. Clone this repo 

```
git clone https://gitlab.com/1enu/congo-custom-template
```

2. Init hugo without a config 

```
hugo mod init page --config "nothing"
```

3. Launch

```
hugo server
```